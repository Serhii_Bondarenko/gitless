import gulp from "gulp"
import gulpSass from "gulp-sass"
import {deleteAsync} from 'del'
import imagemin from 'gulp-imagemin';
import dartSass from "sass";
import CleanCSS from "gulp-clean-css";
import rename from "gulp-rename";
import concat from "gulp-concat";
import autoprefixer from "gulp-autoprefixer";
import minifyjs from "gulp-js-minify";

import browsersync from "browser-sync";



const sass = gulpSass(dartSass);

const copyCss = () => {
    return gulp
        .src("./src/scss/main.scss")
        .pipe(sass({ outputStyle: "expanded" }))
        .pipe(concat("styles.css"))
        .pipe(autoprefixer({overrideBrowserslist: ['last 5 versions']}))
        .pipe(CleanCSS({ compatibility: "ie8" }).on('error', sass.logError))
        .pipe(
            rename({
                suffix: ".min",
                extname: ".css",
            })
        )
        .pipe(gulp.dest("./dist/css"))
};

const copyHtml = () => {
    return gulp
        .src("./index.html")
        .pipe(gulp.dest("./dist"))

};

const copyImgs = () => {
    return gulp
        .src("./src/img/*")
        .pipe(imagemin())
        .pipe(gulp.dest("./dist/img"))
};


const copyJs = () => {
    return gulp
        .src("./src/js/*.js")
        .pipe(concat("script.js"))
        .pipe(minifyjs())
        .pipe(
            rename({
                suffix: ".min",
                extname: ".js",
            })
        )
        .pipe(gulp.dest("./dist/js"))
}

const cleanDist = () => deleteAsync('./dist');




// const watchJs = () => {
//     gulp.watch("./src/js/*.js", buildJs);
// }
//
//
// const watchCSS = () => {
//     gulp.watch("./src/scss/**/*.scss", buildCss)
// }
//
// const watchHTML = () => {
//     gulp.watch("./src/*.html", buildHtml)
// }
//
// const watchImgs = () => {
//
//     gulp.watch("./src/img", builgImgs)
// }

// gulp.task("default", gulp.series( watchCSS, watchHTML,watchJs, watchImgs, buildJs));
// gulp.task("default", gulp.parallel(clearImg));
const build = gulp.series(cleanDist, gulp.parallel(copyHtml, copyImgs, copyCss, copyJs));

exports.build = build